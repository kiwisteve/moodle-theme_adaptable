@theme @theme_adaptable @javascript
Feature: Expandable categories
  To select courses in adaptable theme I need to use the expandable categories

  Background:
    Given the following "categories" exist:
      | name  | idnumber | category |
      | Cat 1 | CAT1     | 0        |
      | Cat 2 | CAT2     | 0        |
    And the following "courses" exist:
      | fullname | shortname | category |
      | Course 1 | C1        | CAT1 |
      | Course 2 | C2        | CAT1 |
      | Course 3 | C3        | CAT2 |
      | Course 4 | C4        | CAT2 |
      | Course 5 | C5        | CAT2 |
    And the following "users" exist:
      | username | firstname | lastname | email |
      | teacher1 | Teacher | 1 | teacher1@example.com |
      | student1 | Student | 1 | student1@example.com |
    And the following "course enrolments" exist:
      | user | course | role |
      | teacher1 | C1 | editingteacher |
      | student1 | C1 | student |
    And I log in as "admin"
    And I change theme to Adaptable
    And I change frontpage display to List of categories
    And I log out

  Scenario: Teacher can expand category
    And I log in as "teacher1"
    And I am on site homepage
    And I click on "div.content div.subcategories div.category loaded collapsed [data-categoryid=2]" "css_element"
    And I should see "Course 3"
    And I should see "Course 4"
    And I should see "Course 5"
    And I log out

  Scenario: Student can expand category
    And I log in as "student1"
    And I am on site homepage
    And I click on "div.content div.subcategories div.category loaded collapsed [data-categoryid=2]" "css_element"
    And I should see "Course 3"
    And I should see "Course 4"
    And I should see "Course 5"
    And I log out
