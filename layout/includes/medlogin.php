<!--login prompt -->
    <div id="theloginbox" class="container">
        <div id="theloginbox-internal" class="text-center">
            <div class="row-fluid">
                <div class="">
	               <h4>Please Login with your University username &amp; password</h4>
		        </div>     
                <div class="">
				    <form class="loginform" id="login" method="post" action="login/index.php" autocomplete="off">			
					   <div class="c1 fld username" id="">
						<input type="text" name="username" id="login_username" value="" placeholder="Username">
						<input type="password" name="password" id="login_password" value="" autocomplete="off" placeholder="Password">
						<span class="c1 btw"><input type="submit" value="Log in"></span>
					   </div>
                    </form>
                    <div>
                        <a href="login/forgot_password.php">Lost or forgotten your password?</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!---->
