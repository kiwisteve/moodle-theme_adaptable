<!-- BRENDONS MENU STARTS HERE ---------------------->          
<div id="brnav-container">

  <!-- HELP Menu -->  
  <div id="helpmenu" class="collapse brnav-block"> 
    <ul class="container-fluid mymenu" >
      <li>
        <div class="row-fluid" >
          <ul class="unstyled span12">
            <li><h2 class="brnav-heading">Help Navigation</h2></li>
            <li class="home">
	      <a title="Contact Support- email med.moodle@otago.ac.nz" href="mailto:med.moodle@otago.ac.nz?Subject=Help with Moodle&body=In order to help you, we need details of your browser and operating system. %0D%0DPLEASE DO THE FOLLOWING%0D%0D1. go to http://www.otago.ac.nz/checkinfo %0D%0D2. COPY the information from the first white box. %0D%0D3. paste the results into this email. %0D%0DNow please describe the issue you are experiencing:">Contact Support- email med.moodle@otago.ac.nz</a>
              <button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#helpmenu">Close X</button>
	    </li>
            <li class="home_divider"></li>
          </ul>
        </div>
        <div class="row-fluid">  
            <ul class="unstyled span4 mylist">
              <li class="programme">Student Resources</li>
              <li><a title="Moodle Support" href="https://medschool.otago.ac.nz/course/view.php?id=214">Moodle Support</a></li>
              <li><a title="Student Support" href="https://medschool.otago.ac.nz/course/view.php?id=1309">Student Support</a></li>
              <li><a title="MB ChB Policies and Guidelines" href="https://www.otago.ac.nz/medicine/current-students/resources/policies-guides/index.html">MB ChB Policies and Guidelines</a></li>
          </ul> 

          <ul class="unstyled span4 mylist"> 
            <li class="programme">Staff Resources</li>
            <li><a title="Moodle Help Site" href="https://help.otago.ac.nz/moodle/">Moodle Help Site</a></li>
            <li><a title="Teaching and Learning in Health Sciences" href="http://learning.healthsci.otago.ac.nz">Teaching and Learning in Health Sciences</a></li>
            <li><a title="Examples Course" href="https://medschool.otago.ac.nz/course/view.php?id=479">Examples Course</a></li>
            <li><a title="ELM Tutor Resources" href="https://medschool.otago.ac.nz/course/view.php?id=224">ELM Tutor Resources</a></li>
          </ul>

          <ul class="unstyled span4 mylist">
            <li class="programme">Support</li>
            <li><a title="Contact Support- email med.moodle@otago.ac.nz" href="mailto:med.moodle@otago.ac.nz?Subject=Help with Moodle&body=In order to help you, we need details of your browser and operating system. %0D%0DPLEASE DO THE FOLLOWING%0D%0D1. go to http://www.otago.ac.nz/checkinfo %0D%0D2. COPY the information from the first white box. %0D%0D3. paste the results into this email. %0D%0DNow please describe the issue you are experiencing:">Contact Support- email med.moodle@otago.ac.nz</a></li>
            <li><br /></li>
            <li class="programme">Other Moodles</li>
            <li><a title="For Non-Medical Programme Health Sciences Courses" href="https://hsmoodle.otago.ac.nz">HS Moodle</a></li>
            <li><a title="For Dept of Anatomy Courses" href="https://anatomystudent.otago.ac.nz">Anatomy Moodle</a></li>
            <li><a title="For Medical Progarmme Examinations" href="https://medtest.otago.ac.nz">MedTest Moodle</a></li>
          </ul>
        </div>
      </li>
      
    </ul>  
  </div>


  <!-- old STAFF Menu   
  <div id="staffmenu" class="collapse brnav-block"> 
    <ul class="container-fluid mymenu" >
      <li>
        <div class="row-fluid" >
          <ul class="unstyled span12">
            <li><h2 class="brnav-heading">Staff Support Navigation</h2></li>
            <li class="home">
	      <a title="Staff Teaching and Learning Support" href="https://medschool.otago.ac.nz/course/view.php?id=224">Staff Teaching and Learning Support</a>
              <button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#staffmenu">Close X</button>
	    </li>
            <li class="home_divider"></li>
          </ul>
        </div>
        <div class="row-fluid">  

          <ul class="unstyled span4 mylist">
            <li class="programme">Moodle Resources</li>
            <li><a title="Moodle Staff Support" href="https://medschool.otago.ac.nz/course/view.php?id=143">Moodle Staff Support</a></li>
            <li><a title="Moodle Handbook" href="http://facmed.otago.ac.nz/elearning/wp-content/uploads/sites/6/2010/12/using_moodle_2e.pdf">Moodle Handbook for staff</a></li>
            <li><a title="Video Tutorials for staff" href="https://medschool.otago.ac.nz/course/view.php?id=143#section-2">Video Tutorials for staff</a></li>
          </ul> 

          <ul class="unstyled span4 mylist"> 
            <li class="programme">eLearning Resources</li>
            <li><a title="Examples Course" href="https://medschool.otago.ac.nz/course/view.php?id=479">Examples Course</a></li>
            <li><a title="How To" href="http://elearning.healthsci.otago.ac.nz">How To</a></li>
          </ul>

          <ul class="unstyled span4 mylist">
            <li class="programme">Support</li>
            <li>
	      <a title="Contact Support- email med.moodle@otago.ac.nz" href="mailto:med.moodle@otago.ac.nz?Subject=Help with Moodle&body=In order to help you, we need details of your browser and operating system. %0D%0DPLEASE DO THE FOLLOWING%0D%0D1. go to http://www.otago.ac.nz/checkinfo %0D%0D2. COPY the information from the first white box. %0D%0D3. paste the results into this email. %0D%0DNow please describe the issue you are experiencing:">Contact Support- email med.moodle@otago.ac.nz</a>
	    </li>
            <li><a title="PostGrad" href="https://hsmoodle.otago.ac.nz">PostGrad</a></li>
          </ul>

        </div>
      </li>
      
    </ul>  
  </div>
  -->

  <!-- ELM 2 Menu -->  
  <div id="elm2" class="collapse brnav-block"> 
    <ul class="container-fluid mymenu" >
      <li>
        <div class="row-fluid" >
          <ul class="unstyled span12">
            <li><h2 class="brnav-heading">ELM 2 Navigation</h2></li>
            <li class="home"><a title="ELM 2 Home" href="https://medschool.otago.ac.nz/course/view.php?id=1427">ELM 2 Home</a>
              <button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#elm2">Close X</button>
	  		</li>
            <li class="home_divider"></li>
          </ul>
        </div>
        <div class="row-fluid">  

          <ul class="unstyled span3 mylist">
            <!-- elm 2 BLOCK MODULES -->        
            <li class="programme"><div>Block Modules</div></li>
            <li><a title="Psychological Medicine (Block)" href="https://medschool.otago.ac.nz/course/view.php?id=1408">Psychological Medicine (Block)</a></li>
            <li><a title="Cardiovascular System" href="https://medschool.otago.ac.nz/course/view.php?id=1406">Cardiovascular System</a></li>
            <li><a title="Gastrointestinal System" href="https://medschool.otago.ac.nz/course/view.php?id=1410">Gastrointestinal System</a></li>
            <li><a title="Musculoskeletal System" href="https://medschool.otago.ac.nz/course/view.php?id=1409">Musculoskeletal System</a></li>
            <li><a title="Respiratory System" href="https://medschool.otago.ac.nz/course/view.php?id=1407">Respiratory System</a></li>
          </ul> 

          <ul class="unstyled span3 mylist">
            <!-- elm 2 VERTICAL MODULES --> 
            <li class="programme">Vertical Modules</li>   
            <li><a title="Blood" href="https://medschool.otago.ac.nz/course/view.php?id=1411">Blood</a></li>
            <li><a title="Cancer" href="https://medschool.otago.ac.nz/course/view.php?id=1412">Cancer</a></li>
            <li><a title="Clinical Pharmacology" href="https://medschool.otago.ac.nz/course/view.php?id=1420">Clinical Pharmacology</a></li>
            <li><a title="EBP" href="https://medschool.otago.ac.nz/course/view.php?id=1413">EBP</a></li>
            <li><a title="Palliative Medicine and End of Life Care" href="https://medschool.otago.ac.nz/course/view.php?id=1414">Palliative Medicine and End of Life Care</a></li>
            <li><a title="Ethics" href="https://medschool.otago.ac.nz/course/view.php?id=1415">Ethics</a></li>
            <li><a title="Genetics" href="https://medschool.otago.ac.nz/course/view.php?id=1416">Genetics</a></li>
            <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1417">Hauora Māori</a></li>
            <li><a title="Infection &amp; Immunology" href="https://medschool.otago.ac.nz/course/view.php?id=1418">Infection &amp; Immunology</a></li>
            <li><a title="Pathology" href="https://medschool.otago.ac.nz/course/view.php?id=1419">Pathology</a></li>
            <li><a title="Professional Development" href="https://medschool.otago.ac.nz/course/view.php?id=1421">Professional Development</a></li>
            <li><a title="Psychological Medicine (Vertical)" href="https://medschool.otago.ac.nz/course/view.php?id=1422">Psychological Medicine (Vertical)</a></li>
            <li><a title="Public Health" href="https://medschool.otago.ac.nz/course/view.php?id=1423">Public Health</a></li>
          </ul>

          <ul class="unstyled span3 mylist">
            <!-- elm 2 PROGRAMME MODULES -->
            <li class="programme">Programmes</li>
            <li><a title="Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1399">Clinical Skills</a></li>
            <li><a title="Early Professional Experience" href="https://medschool.otago.ac.nz/course/view.php?id=1425">Early Professional Experience</a></li>
            <li><a title="Integrated Cases" href="https://medschool.otago.ac.nz/course/view.php?id=1424">Integrated Cases</a></li>
          </ul>

          <ul class="unstyled span3 mylist">
            <!-- elm 2 PROGRAMME MODULES -->
            <li class="programme">Other</li>
            <li><a title="2017 HSFY" href="https://medschool.otago.ac.nz/course/view.php?id=1490">2017 HSFY</a></li>
            <li><a title="2018 ResearchSmart" href="https://medschool.otago.ac.nz/course/view.php?id=1471">2018 ResearchSmart</a></li>
            <li><a title="Revision Resources" href="https://medschool.otago.ac.nz/course/view.php?id=1531">Revision Resources</a></li>
          </ul>
        </div>
      </li>
      
    </ul>  
  </div>

  <!-- ELM 3 Menu -->  
  <div id="elm3" class="collapse brnav-block"> 
    <ul class="container-fluid mymenu" >
      <li>
        <div class="row-fluid" >
          <ul class="unstyled span12">
            <li><h2 class="brnav-heading">ELM 3 Navigation</h2></li>
            <li class="home"><a title="ELM 3 Home" href="https://medschool.otago.ac.nz/course/view.php?id=1258">ELM 3 Home</a>
              <button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#elm3">Close X</button>
		    </li>
	        <li class="home_divider"></li>
          </ul>
        </div>
        <div class="row-fluid">  

          <ul class="unstyled span3 mylist">
            <!-- elm 3 BLOCK MODULES -->        
            <li class="programme"><div>Block Modules</div></li>
            <li><a title="Psychological Medicine (Block)" href="https://medschool.otago.ac.nz/course/view.php?id=1274">Psychological Medicine (Block)</a></li>
            <li><a title="Cardiovascular System" href="https://medschool.otago.ac.nz/course/view.php?id=1259">Cardiovascular System</a></li>
            <li><a title="Endocrine" href="https://medschool.otago.ac.nz/course/view.php?id=1440"> Endocrine</a></li>
            <li><a title="Gastrointestinal System" href="https://medschool.otago.ac.nz/course/view.php?id=1260">Gastrointestinal System</a></li>
            <li><a title="Metabolism Nutrition " href="https://medschool.otago.ac.nz/course/view.php?id=1438"> Metabolism Nutrition</a></li>

            <li><a title="Musculoskeletal System" href="https://medschool.otago.ac.nz/course/view.php?id=1261">Musculoskeletal System</a></li>
            <li><a title="Nervous System " href="https://medschool.otago.ac.nz/course/view.php?id=1436"> Nervous System</a></li>
            <li><a title="RCA" href="https://medschool.otago.ac.nz/course/view.php?id=1437"> RCA</a></li>
            <li><a title="RDA" href="https://medschool.otago.ac.nz/course/view.php?id=1441"> RDA</a></li>
            <li><a title="Renal" href="https://medschool.otago.ac.nz/course/view.php?id=1439"> Renal</a></li>

            <li><a title="Respiratory System" href="https://medschool.otago.ac.nz/course/view.php?id=1277">Respiratory System</a></li>
          </ul> 

          <ul class="unstyled span3 mylist">
            <!-- elm 3 VERTICAL MODULES --> 
            <li class="programme">Vertical Modules</li>   
            <li><a title="Blood" href="https://medschool.otago.ac.nz/course/view.php?id=1256">Blood</a></li>
            <li><a title="Cancer" href="https://medschool.otago.ac.nz/course/view.php?id=1257">Cancer</a></li>
            <li><a title="Clinical Pharmacology" href="https://medschool.otago.ac.nz/course/view.php?id=1271">Clinical Pharmacology</a></li>
            <li><a title="EBP" href="https://medschool.otago.ac.nz/course/view.php?id=1263">EBP</a></li>
            <li><a title="Palliative Medicine and End of Life Care" href="https://medschool.otago.ac.nz/course/view.php?id=1264">Palliative Medicine and End of Life Care</a></li>
            <li><a title="Ethics" href="https://medschool.otago.ac.nz/course/view.php?id=1266">Ethics</a></li>
            <li><a title="Genetics" href="https://medschool.otago.ac.nz/course/view.php?id=1267">Genetics</a></li>
            <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1268">Hauora Māori</a></li>
            <li><a title="Infection &amp; Immunology" href="https://medschool.otago.ac.nz/course/view.php?id=1269">Infection &amp; Immunology</a></li>
            <li><a title="Interprofessional Education" href="https://medschool.otago.ac.nz/course/view.php?id=1492">IPE</a></li>
            <li><a title="Pathology" href="https://medschool.otago.ac.nz/course/view.php?id=1270">Pathology</a></li>
            <li><a title="Professional Development" href="https://medschool.otago.ac.nz/course/view.php?id=1272">Professional Development</a></li>
            <li><a title="Psychological Medicine (Vertical)" href="https://medschool.otago.ac.nz/course/view.php?id=1273">Psychological Medicine (Vertical)</a></li>
            <li><a title="Public Health" href="https://medschool.otago.ac.nz/course/view.php?id=1275">Public Health</a></li>
          </ul>

          <ul class="unstyled span3 mylist">
            <!-- elm 3 PROGRAMME MODULES -->
            <li class="programme">Programmes</li>
            <li><a title="Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1278">Clinical Skills</a></li>
            <li><a title="Early Professional Experience" href="https://medschool.otago.ac.nz/course/view.php?id=1265">Early Professional Experience</a></li>
            <li><a title="Integrated Cases" href="https://medschool.otago.ac.nz/course/view.php?id=1262">Integrated Cases</a></li>
          </ul>

          <ul class="unstyled span3 mylist">
            <!-- elm 3 PROGRAMME MODULES -->
            <li class="programme">Other</li>
            <li><a title="2016 HSFY" href="https://medschool.otago.ac.nz/course/view.php?id=1286">2016 HSFY</a></li>
            <li><a title="2017 ResearchSmart" href="https://medschool.otago.ac.nz/course/view.php?id=1276">2017 ResearchSmart</a></li>
            <li><a title="Revision Resources" href="https://medschool.otago.ac.nz/course/view.php?id=1531">Revision Resources</a></li>
          </ul>
        </div>
      </li>
      
    </ul>  
  </div>


  <!-- ALM4 Menu -->
  <div id="alm4" class="collapse brnav-block">
    <ul class="container-fluid mymenu">
      <li>
        <div class="row-fluid">
          <ul class="unstyled span12">
            <li><h2 class="brnav-heading">ALM 4 Navigation</h2></li>
            <li class="home"><a title="ALM 4 - Home" href="https://medschool.otago.ac.nz/course/view.php?id=1444">ALM 4 - Home</a><button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#alm4">Close X</button>
	    </li>
            <li class="home_divider"></li>
          </ul>
        </div>
        <div class="row-fluid">

          <ul class="unstyled span4 mylist">
            <!--UOC-->
            <li class="school-title">UOC</li>

            <ul class="unstyled">
              <li class="programme">Block Modules</li>
              <li><a title="Addiction Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1348">Addiction Medicine</a></li>   
              <li><a title="CardioRespiratory, Vascular, Plastic Surgery, Dermatology" href="https://medschool.otago.ac.nz/course/view.php?id=1349">CardioRespiratory, Vascular, Plastic Surgery, Dermatology</a></li>
              <li><a title="General Practice" href="https://medschool.otago.ac.nz/course/view.php?id=1350">General Practice</a></li>
              <li><a title="Healthcare of the Elderly" href="https://medschool.otago.ac.nz/course/view.php?id=1351">Healthcare of the Elderly</a></li>
              <li><a title="Surgery / Emergency Medicine / Gastroenterology / Oncology" href="https://medschool.otago.ac.nz/course/view.php?id=1352">Surgery / Emergency Medicine / Gastroenterology / Oncology</a></li>
              <li><a title="Public Health" href="https://medschool.otago.ac.nz/course/view.php?id=1354">Public Health</a></li>    

              <li><br /></li>

              <li class="programme">Vertical Modules</li>
              <li><a title="Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1355">Clinical Skills</a></li>
              <li><a title="Ethics &amp; Law" href="https://medschool.otago.ac.nz/course/view.php?id=1356">Ethics &amp; Law</a></li>
              <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1329">Hauora Māori</a></li>
              <li><a title="Palliative Medicine and End of Life Care" href="https://medschool.otago.ac.nz/course/view.php?id=1357">Palliative Medicine and End of Life Care</a></li>
              <li><a title="Pathology" href="https://medschool.otago.ac.nz/course/view.php?id=1446">Pathology</a></li>
              <li><a title="Professional Development" href="https://medschool.otago.ac.nz/course/view.php?id=1358">Professional Development</a></li>
              <li><a title="Quality and Safety" href="https://medschool.otago.ac.nz/course/view.php?id=1359">Quality and Safety</a></li>

              <li><br /></li>

              <li class="programme">Other</li>
              <li><a title="Clinical Orientation 2018" href="https://medschool.otago.ac.nz/course/view.php?id=1385">Clinical Orientation 2018</a></li>
            </ul>
	    
          </ul>
          
          <ul class="unstyled span4 mylist">
            <!--DSM-->
            <li class="school-title">DSM</li>
            
            <ul class="unstyled">
              <li class="programme">Block Modules</li>
              <li><a title="Medicine 1" href="https://medschool.otago.ac.nz/course/view.php?id=1662">Medicine 1</a></li>
              <li><a title="Psychological Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1472">Psychological Medicine</a></li>
              <li><a title="Public Health" href="https://medschool.otago.ac.nz/course/view.php?id=1469">Public Health</a></li>
              <li><a title="Surgery" href="https://medschool.otago.ac.nz/course/view.php?id=1468">Surgery</a></li>
              <li><a title="Urban GP / ENT" href="https://medschool.otago.ac.nz/course/view.php?id=1470">Urban GP / ENT</a></li>
              

              <li><br /></li>

              <li class="programme">Vertical Modules</li>
              <li><a title="Clinical Pharmacology" href="https://medschool.otago.ac.nz/course/view.php?id=1474">Clinical Pharmacology</a></li>
              <li><a title="Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1473">Clinical Skills</a></li>
              <li><a title="Ethics" href="https://medschool.otago.ac.nz/course/view.php?id=1488">Ethics</a></li>
              <li><a title="Evidence Based Practice" href="https://medschool.otago.ac.nz/course/view.php?id=1477">Evidence Based Practice</a></li>
              <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1486">Hauora Māori</a></li>
              <li><a title="Pathology" href="https://medschool.otago.ac.nz/course/view.php?id=1487">Pathology</a></li>
              <li><a title="Palliative Medicine and End of Life Care" href="https://medschool.otago.ac.nz/course/view.php?id=1476">Palliative Medicine and End of Life Care</a></li>
              <li><a title="Professional Development" href="https://medschool.otago.ac.nz/course/view.php?id=1479">Professional Development</a></li>
              <li><a title="Radiology" href="https://medschool.otago.ac.nz/course/view.php?id=1478">Radiology</a></li>

              <li><br /></li>

              <li class="programme">Other</li>
              <li><a title="Whole Class Teaching" href="https://medschool.otago.ac.nz/course/view.php?id=1475">Whole Class Learning</a></li>
              <li><a title="Pregnancy Long Case" href="https://medschool.otago.ac.nz/course/view.php?id=1345">Pregnancy Long Case</a></li>
              <li><a title="Paediatric Chronic Longitudinal Case" href="https://medschool.otago.ac.nz/course/view.php?id=1518">Paediatric Chronic Longitudinal Case</a></li>   
            </ul>

          </ul>
          
          <ul class="unstyled span4 mylist">
            <!--UOW-->
            <li class="school-title">UOW</li>
            
            <ul class="unstyled">
              
              <li class="programme">Block Modules</li>
              <li><a title="Advanced Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1451">Advanced Clinical Skills</a></li>
              <li><a title="General Practice and Public Health" href="https://medschool.otago.ac.nz/course/view.php?id=1450">General Practice and Public Health</a></li>
              <li><a title="Medicine and Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1449">Medicine and Clinical Skills</a></li>
              <li><a title="Surgical and Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1448">Surgery and Clinical Skills</a></li>

              <li><br /></li>

              <li class="programme">Vertical Modules</li>
              <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1453">Hauora Māori</a></li>
              <li><a title="Pathology" href="https://medschool.otago.ac.nz/course/view.php?id=1341">Pathology</a></li>
              <li><a title="Professional Development and Ethics" href="https://medschool.otago.ac.nz/course/view.php?id=1452">Professional Development and Ethics</a></li>
              <li><a title="Palliative Medicine and End of Life Care" href="https://medschool.otago.ac.nz/course/view.php?id=1397">Palliative Medicine and End of Life Care</a></li>

            </ul>

          </ul>
          
          <!-- Normal Previous Years-->
          <!--<div class="row-fluid">-->
          <ul class="unstyled span4 mylist">
            <li class="school-title">Other</li>
            <li class="">
              <a href="https://medschool.otago.ac.nz/course/index.php?categoryid=150" title="Previous Years">2016-2017 ELM</a>
            </li>
            <li><a title="Revision Resources" href="https://medschool.otago.ac.nz/course/view.php?id=1531">Revision Resources</a></li>
          </ul>
	</div>
      </li>
    </ul>
  </div>


  <!-- ALM5 Menu -->
  <div id="alm5" class="collapse brnav-block">
    
    <!-- ALM 5 Sub-menu -->
    <ul class="container-fluid mymenu">
      <li>
	<div class="row-fluid">
          <ul class="unstyled span12">
            <li><h2 class="brnav-heading">ALM 5 Navigation</h2></li>
            <li class="home">
	      <a title="ALM 5 - Home" href="https://medschool.otago.ac.nz/course/view.php?id=1445">ALM 5 - Home</a>
	      <button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#alm5">Close X</button>
	    </li>
	    <li class="home_divider"></li>
          </ul>
	</div>
	<div class="row-fluid">
          <ul class="unstyled span4 mylist">
            <!--UOC-->
            <li class="school-title">UOC</li>
            
            <ul class="unstyled">
              
              <li class="programme">Block Modules</li>
              <li><a title="Advanced Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1360">Advanced Medicine</a></li>
              <li><a title="Orthopaedics and Advanced Surgery" href="https://medschool.otago.ac.nz/course/view.php?id=1361">Orthopaedics and Advanced Surgery</a></li>
              <li><a title="Paediatrics" href="https://medschool.otago.ac.nz/course/view.php?id=1362">Paediatrics</a></li>
              <li><a title="Psychological Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1363">Psychological Medicine</a></li>
              <li><a title="Womens Health and Developmental Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1364">Womens Health and Developmental Medicine</a></li>  
              
              <li><br /></li>
              
              <li class="programme">Vertical Modules</li>
              <li><a title="Addiction Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1365">Addiction Medicine</a></li>
              <li><a title="Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1366">Clinical Skills</a></li>
              <li><a title="Ethics & Law" href="https://medschool.otago.ac.nz/course/view.php?id=1367">Ethics & Law</a></li>
              <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1342">Hauora Māori</a></li>
              <li><a title="Pathology" href="https://medschool.otago.ac.nz/course/view.php?id=1447">Pathology</a></li>
              <li><a title="Clinical Pharmacology" href="https://medschool.otago.ac.nz/course/view.php?id=1368">Clinical Pharmacology</a></li>
              <li><a title="Professional Development" href="https://medschool.otago.ac.nz/course/view.php?id=1369">Professional Development</a></li>
              <li><a title="Quality and Safety" href="https://medschool.otago.ac.nz/course/view.php?id=1370">Quality and Safety</a></li>
            </ul>
          </ul>
          
          <ul class="unstyled span4 mylist">
            <!--DSM-->
            <li class="school-title">DSM</li>
            
            <ul class="unstyled">
              <li class="programme">Block Modules</li>
              <li><a title="Child Health & Reproductive Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1400">Child Health & Reproductive Medicine</a></li>
              <li><a title="Medicine 2" href="https://medschool.otago.ac.nz/course/view.php?id=1484">Medicine 2</a></li>
              <li><a title="Musculoskeletal, Anaesthesia and Intensive Care" href="https://medschool.otago.ac.nz/course/view.php?id=1482">Musculoskeletal, Anaesthesia and Intensive Care</a></li>
              <li><a title="Rural GP" href="https://medschool.otago.ac.nz/course/view.php?id=1481">Rural GP</a></li>
              
              <li><br /></li>
              
              <li class="programme">Vertical Modules</li>
			  <li><a title="Clinical Pharmacology" href="https://medschool.otago.ac.nz/course/view.php?id=1474">Clinical Pharmacology</a></li>
              <li><a title="Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1473">Clinical Skills</a></li>
              <li><a title="Ethics" href="https://medschool.otago.ac.nz/course/view.php?id=1488">Ethics</a></li>
              <li><a title="Evidence Based Practice" href="https://medschool.otago.ac.nz/course/view.php?id=1477">Evidence Based Practice</a></li>
              <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1485">Hauora Māori</a></li>
              <li><a title="Pathology" href="https://medschool.otago.ac.nz/course/view.php?id=1487">Pathology</a></li>
              <li><a title="Palliative Medicine and End of Life Care" href="https://medschool.otago.ac.nz/course/view.php?id=1476">Palliative Medicine and End of Life Care</a></li>
              <li><a title="Professional Development" href="https://medschool.otago.ac.nz/course/view.php?id=1480">Professional Development</a></li>
              <li><a title="Radiology" href="https://medschool.otago.ac.nz/course/view.php?id=1478">Radiology</a></li>

              <li><br /></li>

              <li class="programme">Other</li>
              <li><a title="Whole Class Teaching" href="https://medschool.otago.ac.nz/course/view.php?id=1489">Whole Class Learning</a></li>
              <li><a title="Paediatric Chronic Longitudinal Case" href="https://medschool.otago.ac.nz/course/view.php?id=1494">Paediatric Chronic Longitudinal Case</a></li>                       
            </ul>
          </ul>
          
          <ul class="unstyled span4 mylist">
            <!--UOW-->
            <li class="school-title">UOW</li>
            
            <ul class="unstyled">
              
              <li class="programme">Block Modules</li>
              <li><a title="Primary Health Care and General Practice" href="https://medschool.otago.ac.nz/course/view.php?id=1455">Primary Health Care and General Practice</a></li>
              <li><a title="Musculoskeletal and Skin" href="https://medschool.otago.ac.nz/course/view.php?id=1457">Musculoskeletal and Skin</a></li>
              <li><a title="Psychological Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1459">Psychological Medicine</a></li>
              <li><a title="General Medicine and Subspecialities" href="https://medschool.otago.ac.nz/course/view.php?id=1456">General Medicine and Subspecialities</a></li>
              <li><a title="Child and Adolescent Health" href="https://medschool.otago.ac.nz/course/view.php?id=1458">Child and Adolescent Health</a></li>
              <li><a title="Women’s Health" href="https://medschool.otago.ac.nz/course/view.php?id=1460">Women’s Health</a></li>                        
              
              <li><br /></li>
              
              <li class="programme">Vertical Modules</li>
              <li><a title="Clinical Pharmacology" href="https://medschool.otago.ac.nz/course/view.php?id=1461">Clinical Pharmacology</a></li>
              <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1465">Hauora Māori</a></li>
              <li><a title="Radiology" href="https://medschool.otago.ac.nz/course/view.php?id=1464">Radiology</a></li>
              <li><a title="Pathology" href="https://medschool.otago.ac.nz/course/view.php?id=1463">Pathology</a></li>
              <li><a title="Professional Development and Ethics" href="https://medschool.otago.ac.nz/course/view.php?id=1462">Professional Development and Ethics</a></li>
              <li><a title="Palliative Medicine and End of Life Care" href="https://medschool.otago.ac.nz/course/view.php?id=1344">Palliative Medicine and End of Life Care</a></li>

            </ul>
          </ul>
	</div>
	<div class="row-fluid">
          
          <ul class="unstyled span4 mylist">
            <li class="school-title">RMIP</li>
            <li><a title="Hauora Māori for RMIP" href="https://medschool.otago.ac.nz/course/view.php?id=1384">Hauora Māori for RMIP</a></li>
            <li><a title="RMIP Home" href="https://medschool.otago.ac.nz/course/view.php?id=1483">RMIP Home</a></li>
          </ul>
          
          <!-- Normal Previous Years-->
          <!--<div class="row-fluid">-->
          <ul class="unstyled span4 mylist">
            <li class="school-title">Other</li>
            <li><a href="https://medschool.otago.ac.nz/course/index.php?categoryid=128" title="Previous Years">2015-2016 ELM</a></li>
            <li><a href="https://medschool.otago.ac.nz/course/index.php?categoryid=151" title="Previous Years">2017 ALM 4</a></li>
            <li><a title="Revision Resources" href="https://medschool.otago.ac.nz/course/view.php?id=1531">Revision Resources</a></li>
          </ul>
	</div>	
      </li>
    </ul>  
  </div>


  <!-- ALM6 Menu -->
  <div id="alm6" class="collapse brnav-block">
    <ul class="container-fluid mymenu">
      <li>
	<div class="row-fluid">
          <ul class="unstyled span12">
            <li><h2 class="brnav-heading">ALM 6 Navigation</h2></li>
            <li class="home">
	      <a title="ALM 6 - Home" href="https://medschool.otago.ac.nz/course/view.php?id=1613">ALM 6 - Home</a>
	      <button type="button" class="btn collapsed bbnav brmenu-close-btn pull-right" data-toggle="collapse" data-target="#alm6">Close X</button>
	    </li>
            <li class="home_divider"></li>
          </ul>
	</div>
	<div class="row-fluid">
          <ul class="unstyled span4 mylist">
            <!--UOC-->
            <li class="school-title">UOC</li>
            <ul class="unstyled">

              <li class="programme">Block Modules</li>
              <li><a title="Elective" href="https://medschool.otago.ac.nz/course/view.php?id=1541">Elective</a></li>
              <li><a title="Paediatrics" href="https://medschool.otago.ac.nz/course/view.php?id=1543">Paediatrics</a></li>
              <li><a title="Obstetrics & Gynaecology" href="https://medschool.otago.ac.nz/course/view.php?id=1544">Obstetrics &amp; Gynaecology</a></li>
              <li><a title="Psychological Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1545">Psychological Medicine</a></li>
              <li><a title="Critical Care" href="https://medschool.otago.ac.nz/course/view.php?id=1546">Critical Care</a></li>
              <li><a title="Surgery" href="https://medschool.otago.ac.nz/course/view.php?id=1547">Surgery</a></li>
              <li><a title="Selective" href="https://medschool.otago.ac.nz/course/view.php?id=1548">Selective</a></li>
              <li><a title="General Practice" href="https://medschool.otago.ac.nz/course/view.php?id=1549">General Practice</a></li>
              <li><a title="Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1550">Medicine</a></li>
	      
              <li><br /></li>

              <li class="programme">Vertical Modules</li>
              <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1530">Hauora Māori</a></li>
              <li><a title="Professional Development" href="https://medschool.otago.ac.nz/course/view.php?id=1551">Professional Development</a></li>
              <li><a title="Friday Afternoon Teaching" href="https://medschool.otago.ac.nz/course/view.php?id=1553">Friday Afternoon Teaching</a></li>
              <li><a title="Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1542">Clinical Skills</a></li>
            </ul>
          </ul>
          
          <ul class="unstyled span4 mylist">
            <!--DSM-->
            <li class="school-title">DSM</li>
            
            <ul class="unstyled">
              <li class="programme">Block Modules</li>
              <li><a title="Elective" href="https://medschool.otago.ac.nz/course/view.php?id=1622">Elective</a></li>
              <li><a title="Obstetrics & Gynaecology" href="https://medschool.otago.ac.nz/course/view.php?id=1624">Obstetrics & Gynaecology</a></li>
              <li><a title="Paediatrics" href="https://medschool.otago.ac.nz/course/view.php?id=1625">Paediatrics</a></li>
              <li><a title="Psychological Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1621">Psychological Medicine</a></li>
              <li><a title="Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1623">Medicine</a></li>
              <li><a title="Surgery" href="https://medschool.otago.ac.nz/course/view.php?id=1626">Surgery</a></li>
              <li><a title="Community – Evaluation – Outpatients" href="https://medschool.otago.ac.nz/course/view.php?id=1620">Community – Evaluation – Outpatients</a></li>
              <li><a title="Critical Care and Emergency Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1616">Critical Care and Emergency Medicine</a></li>
              
              <li><br /></li>
              <li class="programme">Vertical Modules</li>
			  <li><a title="Clinical Pharmacology" href="https://medschool.otago.ac.nz/course/view.php?id=1474">Clinical Pharmacology</a></li>
              <li><a title="Clinical Skills" href="https://medschool.otago.ac.nz/course/view.php?id=1473">Clinical Skills</a></li>
              <li><a title="Ethics" href="https://medschool.otago.ac.nz/course/view.php?id=1488">Ethics</a></li>
              <li><a title="Evidence Based Practice" href="https://medschool.otago.ac.nz/course/view.php?id=1477">Evidence Based Practice</a></li>
              <li><a title="Hauora Māori" href="https://medschool.otago.ac.nz/course/view.php?id=1485">Hauora Māori</a></li>
              <li><a title="Pathology" href="https://medschool.otago.ac.nz/course/view.php?id=1487">Pathology</a></li>
              <li><a title="Palliative Medicine and End of Life Care" href="https://medschool.otago.ac.nz/course/view.php?id=1476">Palliative Medicine and End of Life Care</a></li>
              <li><a title="Professional Development" href="https://medschool.otago.ac.nz/course/view.php?id=1480">Professional Development</a></li>
              <li><a title="Radiology" href="https://medschool.otago.ac.nz/course/view.php?id=1478">Radiology</a></li>
              <li><br /></li>

              <!--<li class="programme">Other</li>-->
              <!--<li><a title="2015d5 Whole Class Teaching" href="https://medschool.otago.ac.nz/course/view.php?id=771">Whole Class Teaching</a></li>-->
            </ul>
          </ul>
          
          <ul class="unstyled span4 mylist">
            <!--UOW-->
            <li class="school-title">UOW</li>
            
            <ul class="unstyled">
              
              <li class="programme">Block Modules</li>
              <li><a title="Elective" href="https://medschool.otago.ac.nz/course/view.php?id=1602">Elective</a></li>
              <li><a title="Primary Health Care & General Practice" href="https://medschool.otago.ac.nz/course/view.php?id=1608">Primary Health Care & General Practice</a></li>
              <li><a title="Paediatrics" href="https://medschool.otago.ac.nz/course/view.php?id=1605">Paediatrics</a></li>
              <li><a title="Obstetrics & Gynaecology" href="https://medschool.otago.ac.nz/course/view.php?id=1604">Obstetrics &amp; Gynaecology</a></li>
              <li><a title="Emergency Medicine / Acute Care" href="https://medschool.otago.ac.nz/course/view.php?id=1601">Emergency Medicine / Acute Care</a></li>
              <li><a title="Surgery" href="https://medschool.otago.ac.nz/course/view.php?id=1610">Surgery</a></li>
              <li><a title="Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1603">Medicine</a></li>
              <li><a title="Psychological Medicine" href="https://medschool.otago.ac.nz/course/view.php?id=1609">Psychological Medicine</a></li>

              <li><br /></li>

              <li class="programme">Vertical Modules</li>
              <li><a title="Professional Development & Ethics" href="https://medschool.otago.ac.nz/course/view.php?id=1607">Professional Development & Ethics</a></li>
              <li><a title="Palliative Medicine & End of Life Care" href="https://medschool.otago.ac.nz/course/view.php?id=1606">Palliative Medicine & End of Life Care</a></li>

              <li><br /></li>        

              <!--<li class="programme">Other</li>-->
            </ul>
          </ul>
	</div>
	<div class="row-fluid">        
          <!-- Normal Previous Years-->
          <!--<div class="row-fluid">-->
          <ul class="unstyled span4 mylist">
            <li class="school-title-sm">Cross-campus</li>
            <li><a title="TIs as Teachers by Distance Learning" href="https://medschool.otago.ac.nz/course/view.php?id=1194">TIs as Teachers by Distance Learning</a></li>
            <li><a title="Prepared for Practice" href="https://medschool.otago.ac.nz/course/view.php?id=1333">Prepared for Practice</a></li>
          </ul>
          <ul class="unstyled span4 mylist">
            <li class="school-title">Other</li>
            <li><a href="https://medschool.otago.ac.nz/course/index.php?categoryid=128" title="2015-6 ELM">2015-2016 ELM</a></li>
            <li><a href="https://medschool.otago.ac.nz/course/index.php?categoryid=151" title="2017 ALM4">2017 ALM 4</a></li>
            <li><a href="https://medschool.otago.ac.nz/course/index.php?categoryid=173" title="2018 ALM5">2018 ALM 5</a></li>
          	<li><a title="Revision Resources" href="https://medschool.otago.ac.nz/course/view.php?id=1531">Revision Resources</a></li>
          </ul>
	</div>          
	
      </li>
    </ul>  
  </div>
